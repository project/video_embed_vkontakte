INTRODUCTION
------------

Video Embed Vkontakte videos into your website using Video Embed Field

REQUIREMENTS
------------

This module requires the following modules:

 * Video Embed Field (https://drupal.org/project/video_embed_field)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.
   Or if your site is managed via Composer, use Composer to download the module.
   composer require drupal/video_embed_vkontakte:^1.0

MAINTAINERS
-----------

Current maintainers:
 * Andriy Malyeyev (slivorezka) - https://www.drupal.org/u/slivorezka
